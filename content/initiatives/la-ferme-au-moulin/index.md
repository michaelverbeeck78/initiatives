---
title: "La Ferme Au Moulin"
date: 2020-07-08T10:03:50+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Vente directe,Maraîchage,Gîte]
summary: >
    La ferme au Moulin est une ferme familiale au coeur de la Hesbaye pleine d'opportunités: Cédric et Emile, passionnés de la terre, produisent + de  70 variétés de fruits et légumes (avec un verger d’ancienne variété de fruits). Leurs valeurs écologiques et leur envie de qualité les ont amenés vers une production biologique. Leur potager est un espace où la faune et la flore se développent en harmonie. Ils vous proposent des légumes anciens, originaux, “classiques” débordants de saveurs!
youtubeId: 
valheureux: false
contact: 
    website: https://www.lafermeaumoulin.be
    mail: info@lafermeaumoulin.be
    facebook: La-Ferme-au-Moulin-543176705701294 
    twitter: 
    instagram: 
locations:
  - name: La Ferme Au Moulin
    address: Rue du Moulin 10<br> 4350 Remicourt
    hours: 
    phone: 
    googlePlaceId: ChIJK2oh_L0DwUcRJZbLvIv8AD0
    latitude: 
    longitude: 
---
# Equipe #

Leur équipe s’est étoffée entre temps avec Dorian (temps plein), Jérome (temps plein) un saisonnier (ancien stagiaire), un étudiant, ...

Céline, amoureuse et passionnée des chevaux depuis toujours vous partagera son expérience et son savoir faire avec les chevaux. Soucieuse du respect de chacun, elle vous fera évoluer dans votre travail avec votre cheval. Du débourrage à la Haute école en passant par la balade, la voltige ou l’obstacle…


# Le lieu # 

Leur ferme sera aussi un lieu d’accueil pour partager d’agréables moments en famille ou entre amis.
Ils organisent des stages, des anniversaires pour enfants, balades à dos d’ânes, en calèche, …

Ils proposent également un hébergement dans un ancien moulin joliment restauré en gîte de 4 personnes.
