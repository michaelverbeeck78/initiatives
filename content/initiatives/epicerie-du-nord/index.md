---
title: "L'Epicerie Du Nord"
date: 2020-04-29T09:29:54+02:00
categories: [Alimentation]
tags: [Circuit court,Vrac,Produits ménagers,Boulangerie,Produits laitiers,Boissons,Epices,Farine,Maraîchage]
summary: "Quel plaisir de rentrer dans ce magasin joliment décoré et regorgeant de produits de toutes sortes. Tous les recoins ont été exploités et rien n’a été oublié: les fruits, légumes, produits laitiers, produits en vrac nombreux, produits ménagers zéro déchets, du pain, des boites à oeufs à foison, les bières locales … sans oublier le sourire bienveillant qui va avec."
youtubeId: 
contact:
    website: ""
    mail: epiceriedunord@outlook.be
    facebook: "epiceriedunord.liege"
    twitter: ""
    instagram: ""
locations:
  - name: "L’épicerie du Nord"
    address: "Rue St Léonard 8, 4000 Liège"   
    hours: ""
    phone: "04 380 38 33"
    latitude: 50.648240
    longitude: 5.586142
    googlePlaceId: "ChIJrX9GRKDwwEcRqXs031Htt08"
---
