---
title: "Cabanon"
date: 2021-08-20T20:08:20+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Vrac,Vente directe]
summary: "Conception et réalisation d’articles en tissus destination «zéro déchet»."
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: "cabanon.steph@gmail.com"
    facebook: "cabanon.zd"
    twitter: 
    instagram: "cabanon.zd"
locations:
  - name: "Cabanon"
    address: "Rue Rogier, 46 <br>4800 Verviers"
    hours: 
    phone: 
    googlePlaceId: "ChIJFWRH4ceLwEcR_abA0VrWMH4"
    latitude: 
    longitude: 
---

Remplacez vos déchets du quotidien par de jolies créations pratiques, colorées et aux dessins modernes et variés !
