---
title: "La Vache a Glace"
date: 2020-07-08T10:10:33+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Glacier, Produits laitiers]
summary: >
    Une glace artisanale à base de produits frais du terroir !
youtubeId: 
valheureux: false
contact: 
    website: http://www.lavacheaglace.be
    mail: 
    facebook: lavacheaglace
    twitter: 
    instagram: 
locations:
  - name: La Vache à Glace
    address: Rue de la Ferme de l'Abbaye 10<br> 4550 Nandrin
    hours: 
    phone: 
    googlePlaceId: ChIJaXAOxilVwEcRGWay73dGoXU
    latitude: 
    longitude: 
---
Moins de 15 minutes s'écoulent entre la traite des vaches (de la ferme voisine) et le début du processus de fabrication de la glace. Ils utilisent des oeufs issus de poules élevées en plein air et certifiés "Agriculture biologique" et exclusivement des fruits frais issus de leur verger, de producteurs locaux et du marché matinal de Liège.


*Photos: Nath P.*
