---
title: "Vins & Elixirs de Franchimont"
date: 2020-07-30T10:05:41+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Boissons,Circuit court]
summary: >
    C’est en 2000, que quatre amis avec l’aide de l’administration communale de Theux, décident de lancer une petite société artisanale. Fort investis dans la vie associative et alchimistes confirmés, ils décident de rehausser l’identité de leur région par une boisson fabriquée sur leur terre ! 
youtubeId: 
valheureux: false
contact: 
    website: http://www.fleurdefranchimont.be/
    mail: info@fleurdefranchimont.be
    facebook: fleurdefranchimont
    twitter: 
    instagram: fleur_de_franchimont
locations:
  - name: 
    address: Rue Charles Rittwéger 2<br>4910 Theux	
    hours: 
    phone: 0478 53 09 92
    googlePlaceId: ChIJJ4qTd0lgwEcRmcmqm76L6Mk 
    latitude: 
    longitude: 
---
C’est sans attendre que, d’échecs en réussites, sortent des cuves: le « vin de Fleur » actuellement rebaptisé la « Fleur de Franchimont » ; l’hydromel ; et plus tard, sous l’impulsion de l’administration communale de Spa, la « Rosée de Spa ». Les palais bien aiguisés et la tête remplie de projets, c’est la « FineFleur » suivi de la « FrancheFleur » qui verront le jour. Ils vous proposent une gamme de produits Theutois bien réussis, pour tous les goûts, qui avec modération, accompagneront vos fêtes et vos repas du début jusqu’à la fin !

C’est pour vous qu'ils préparent leurs délicieux breuvages, dans leurs ateliers situés à Theux.
