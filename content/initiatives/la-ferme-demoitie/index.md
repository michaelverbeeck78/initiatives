---
title: "La Ferme Demoitié"
date: 2021-08-20T17:31:09+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Vente directe,Produits laitiers,Boulangerie,Circuit-court]
summary: "Ferme familiale en agriculture biologique depuis plus de 20 ans avec  production laitière et transformation en yaourts, beurre, maquée, crème fraîche."
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: "fermedemoitie@gmail.com"
    facebook: "magasindelafermeDemoitie"
    twitter: 
    instagram: 
locations:
  - name: 
    address: "Lizin 2 4590 Ouffet<br>4590 Ouffet"
    hours: 
    phone: 0499 23 69 24
    googlePlaceId: ChIJpfRHCx5RwEcRjGS9u78DLdQ 
    latitude: 
    longitude: 
---

Le magasin à la ferme est ouvert le mercredi de 13h30 à 18h30 et le samedi de 9h à 16h et vous propose également un large panel de produits locaux. 
