---
title: "Le Jardin d'Antan"
date: 2020-07-08T10:04:18+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Maraîchage,Vente directe]
summary: >
  La motivation du Jardin d'Antan est de produire et commercialiser des fruits et légumes frais de la région, naturels, pour chaque saison. Pas de fraises en janvier, ni de tomates, mais des topinambours, des courges spaghettis, ou des pommes.
youtubeId: 
valheureux: false
contact: 
    website: https://www.jardindantan.be
    mail: info@jardindantan.be
    facebook: infoJardindantan
    twitter: 
    instagram: 
locations:
  - name: Jardin d'Antan
    address: Grand Route de Liège 6<br> 4162 Anthisnes
    hours: 
    phone: 
    googlePlaceId: ChIJNcB19R9XwEcRytBTNVwYiOA
    latitude: 
    longitude: 
---
 Vous l'avez compris, seule la diversité permet d'apporter des légumes à chaque saison. Cela permet de redécouvrir des goûts et des textures méconnus, mais pas pour autant désagréables. Au jardin d'Antan, ils  récoltent uniquement les marchandises dont nous avons besoin et au moment de leur pleine maturité, ce qui leur évite de passer de nombreux jours en frigo, et peut-être de devoir mûrir sous votre lampe de cuisine...

En venant au Jardin d'Antan, vous soutenez une agriculture locale, de la main d'oeuvre locale et toute une économie rurale.
Vous cautionnez des pratiques respectueuses de l'environnement et l'agriculture paysanne de votre région.
