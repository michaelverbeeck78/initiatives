---
title: "Permavenir"
date: 2020-07-08T10:08:48+02:00
draft: false
longText: false
categories: [zz-autres]
tags: [Média indépendant,Education,Sensibilisation,Potager commun,Permaculture]
summary: >
    Permavenir est une association qui sensibilise à l'écologie à travers des vidéos et dans les écoles. Nous créons des potagers avec les enfants et nous animons des ateliers sur l'alimentation saine, la permaculture et le zéro déchet.
    
    A suivre sur Youtube: https://www.youtube.com/PermavenirTV

youtubeId: tw4HzAGGAGc
valheureux: false
contact: 
    website: https://permavenir.be
    mail: info@permavenir.be
    facebook: Permavenir
    twitter: 
    instagram: 
locations:
  - name: 
    address: 
    hours: 
    phone: 0497 19 62 81
    googlePlaceId: 
    latitude: 
    longitude: 
---