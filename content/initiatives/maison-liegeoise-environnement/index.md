---
title: "Maison Liégeoise de l'Environnement"
date: 2021-08-20T19:52:50+02:00
draft: false
longText: false
categories: ["zz-autres"]
tags: [Sensibilisation,Education, Culture]
summary: "La Maison Liégeoise de l’Environnement, c’est la plus grande librairie naturaliste francophone de Belgique."
youtubeId: 
valheureux: false
contact: 
    website: "http://www.maisondelenvironnement.be"
    mail: "mle.botanique@gmail.com"
    facebook: "librairieMLE"
    twitter: 
    instagram: 
locations:
  - name: "Maison Liégeoise de l'Environnement"
    address: Rue Fusch 3 (DANS le parc du jardin Botanique)<br>4000 Liège
    hours: 
    phone: "042509580"
    googlePlaceId: ChIJqUmPtB76wEcR7-m9adyTfLA
    latitude: 
    longitude: 
---
Elle est le rendez-vous des enseignants et animateurs.
Elle permet aussi le groupement d'associations de défense de la nature.

Vous pouvez y trouver également des produits Kite, Swarovski, Novex, Leica.

Accès du mercredi au samedi.
Ouverture à 12h - fermeture à 17h30 

