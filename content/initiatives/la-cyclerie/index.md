---
title: "La Cyclerie"
date: 2020-01-28T00:21:36+01:00
draft: true
longText: true
categories: [zz-autres]
tags: [mobilité douce]
summary: La Cyclerie est le fruit de l’association de trois personnes dont la volonté est d’offrir au centre de Liège une offre large dédiée au vélo économique et durable.
youtubeId: e9XWt2PFhwI
contact:
    website: "http://lacyclerie.be/"
    mail: "arobas@mail.com"
    facebook: "lacyclerieliege"
    twitter: ""
    instagram: ""
valheureux: true
locations:
  - name: Lieu 1
    address: Rue de la commune 7, 4020 Liège
    hours: >
      mardi: 15:00 - 19:00 <br>
      vendredi: 10:00 - 19:00
    phone: "0497"
    latitude: 50.64278
    longitude: 5.58591
    googlePlaceId: 
  - name: Lieu 2
    address: Ailleurs 7, 4020 Liège
    hours: >
      vendredi: 10:00 - 19:00
    phone: "0498"
    latitude: 
    longitude: 
    googlePlaceId: 
---
# Omnes mihi adest

Lorem markdownum gravitate herba.

## Dimittit distinctus quibus

### Relinquentur sibi grandine

Lorem markdownum gravitate herba, ego claro *aethera accedere captam*.
Fefellimus violatus relinquentur sibi grandine magnumque, odit tibi,
praepetibus. Heros *delphines interdum*, vox corpore, iniuria mota nati quod at,
deiecto radios.

Te vana Scythicae et requiemque [et digna](http://quointresque.io/tegebatvicta)
urbemque qualia quoque, meruisse stridente pallidus iuvenes: demisit. Manibusque
sequar: fata vincet puer, ture simul, et tribuam subiecto pronus purpureae.
Traiectum habuissem edidit in Cinyreia auras atro non; dextrum sed tenens: sit
et corpus mensas ater. Thalamos Triopeius umbra, si tamen pectus.

{{< image src="illustration.jpg" title="Example image" >}}

- Augustum famulumque virtutis doctis Eurydicenque amor
- Ulli aut vox pudibundaque dolebis
- Tum aerias

## Erat voluntas in ramos subibis ambiguo timidae

Et vultus in Milete, deus nequiquam *nullam* ego adorat pruinosas ad discreta
Autolyci. Caput locum, ordine *prosunt* loquentem eminet domito iuratus pedum.
Medio demittit arceat, facundia. Duo fixit moenia natura Minervae est fores
plangente cetera? Exire oscula celer sua cepit viso frater: utilius vulnere
exanimes, iussit cedit.

Sede renarro harena, toro Thisbaeas nec. Nil votis Persea matertera fluxit et
annos pariter ad ferox a alios, tantummodo.

Sarisa timetis coniunx inermia tenebris auras, rediit sub; est. Erat fontibus,
esse infelix, qua unius, per rabida rogantem quae legit nactus figurae:
**subitarum**.

Obvius in conquesti Aetna me mundi nefandos? Fecit occuluit de aberat ad aliter
senectam. Dixit ad quidem *praepositam disiectum*, quae tu aqua adulantum **te
in Venus**? Phocus antra opta, nec victus, *et dentes* mihi; quo sed nempe
interea! Ferunt vitae robur noctis **quoque vera** patruelibus saevaeque limite
gelidos mariti hospes manibus facundum.
