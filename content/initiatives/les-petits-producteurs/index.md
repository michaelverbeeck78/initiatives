---
draft: false
longText: true
title: "Les Petits Producteurs"
date: 2020-02-18T14:41:52+01:00
categories: ["alimentation"]
tags: [Maraîchage,Elevage,Produits laitiers,Boissons,Farine,Boulangerie,Epices,Produits ménagers,Vrac,Circuit court,Commerce équitable,Coopérative]
summary: >
  "Les Petits Producteurs" est une coopérative de magasins d'alimentation locale et/ou bio qui construit un modèle agricole durable et solidaire en vendant les produits de petits producteurs en circuit-court et au prix juste.
youtubeId: y8RA-uyAXuo
contact: 
    website: "https://lespetitsproducteurs.be"
    mail: "info@lespetitsproducteurs.be"
    facebook: "LesPetitsProducteurs.be"
    twitter: ""
    instagram: "lespetitsproducteurs.be"
locations:
  - name: "Les Petits Producteurs, Neuvice"
    address: "En Neuvice 34<br> 4000 Liège"
    phone: "04/358 51 53"
    googlePlaceId: ChIJ30doqg76wEcRQRhEKxBbb4M 
  - name: "Les Petits Producteurs, Quartier des Vennes"
    address: "Avenue Reine Elisabeth 1<br> 4020 Liège"
    phone: "04/226 45 52"
    googlePlaceId: ChIJi1-rZ3v3wEcRHkYWXx_SCiU
  - name: "Les Petits Producteurs, Quartier Sainte-Walburge"
    address: "Rue Sainte-Walburge 22<br> 4000 Liège"
    phone: "04/224 07 41"
    googlePlaceId: ChIJ5S2bZW76wEcRPMRmwiTtJpY
  - name: "Les Petits Producteurs, Visé"
    address: "Rue Haute 34<br> 4600 Visé"
    phone: "04/338 23 11"
    googlePlaceId: ChIJyQJcgVLvwEcRawktdNbuSqw     
---
Concrètement, ce sont des épiceries de quartier à Liège et Visé dans lesquelles vous trouverez des fruits et légumes frais de saison, des aliments en vrac (riz, pâtes, farines, café, céréales, huile d’olive), épicerie fine (confiture, miel), du fromage et des produits laitiers, de la viande, de la bière et du vin, des produits d’entretien en vrac…

Leurs produits sont sélectionnés avec soin (sur base de leur qualité et du projet agricole) et proposés à prix juste en limitant les coûts de fonctionnement : aménagement minimaliste, vrac, gamme réduite. Le producteur fixe le prix, que la coopérative ne négocie jamais. Elle s'engage à long terme avec les producteurs pour leur assurer une stabilité et offrir du soutien en cas de difficultés dans les cultures.

En faisant vos courses dans leurs magasins, vous permettez à nos producteurs locaux de vivre du fruit de leur travail, vous soutenez le développement d’une agriculture plus locale, plus saine et plus humaine.
