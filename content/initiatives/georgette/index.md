---
title: "Georgette"
date: 2021-08-20T18:28:32+02:00
draft: false
longText: false
categories: ["Alimentation"]
tags: [Produits laitiers,Boissons,Farine,Boulangerie,Epices,Produits ménagers,Cosmétiques,Vrac,Vegan,Circuit court,Commerce équitable,Vente en ligne,Monnaie locale,Evénèment,Sensibilisation,Ateliers]
summary: "Georgette est un magasin d’alimentation bio et logique niché sur les hauteurs de Liège, dans le sympathique quartier de Cointe."
youtubeId: 
valheureux: true
contact: 
    website: "https://www.georgette.bio"
    mail: "contact@georgette.bio"
    facebook: "georgettebioshop"
    twitter: 
    instagram: "georgette.bio"
locations:
  - name: "Georgette"
    address: "Place du Batty 11<br>4000 Liège"
    hours: 
    phone: 
    googlePlaceId: ChIJWdA-j0n5wEcRnzfaolE5WKg 
    latitude: 
    longitude: 
---

Les produits sont majoritairement issus de l’agriculture biologique, durable et, dans la mesure du possible, locale. L’équipe de Georgette défend une autre manière de consommer et de s’alimenter. La motivation première est d’accélérer notre transition écologique en accompagnant les clients vers une consommation biologique et/ou locale. C’est pourquoi, plus qu’un commerce, Georgette se veut être un lieu de partage et d’éducation.
