---
title: "Disco Soupe"
date: 2020-07-08T10:07:59+02:00
draft: false
longText: true
categories: [zero-dechets]
tags: [Maraîchage,Vrac,Vegan,Circuit court,Restauration,Evènement,Solidarité,Sensibilisation,Education,Ateliers]
summary: >
    Disco Soupe est un mouvement qui œuvre pour la sensibilisation du grand public à la problématique du gaspillage alimentaire.
youtubeId: 
valheureux: false
contact: 
    website: http://discosoupe.org
    mail: tasteandvisual@gmail.com 
    facebook: discosoupeliege
    twitter: 
    instagram: 
locations:
  - name: Liège
    address: 
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---
Communauté d’intérêt diffusée dans toute la France et sur les quatre continents, elle rassemble citoyens et militants autour de valeurs telles que l’initiative, la bidouille, le partage et la gratuité. L’enjeu: interroger le système et les pratiques liées à notre alimentation. Par la convivialité, l’expérience sensible et sensorielle du cuisiner ensemble et la multiplication des expérimentations dans l’espace public, cette communauté tente de construire chemin faisant un nouveau modèle de militantisme résolument joyeux et positif.

Les *Disco Soupes* (ou Disco Salades, Disco Smoothies etc.) sont des sessions collectives et ouvertes de cuisine de fruits et légumes rebuts ou invendus dans une ambiance musicale et festive. Les soupes, salades, jus de fruits ou smoothies ainsi confectionnés sont ensuite redistribués à tous gratuitement ou à prix libre.Les Disco Soupes permettent l’éducation à une cuisine saine et goûtue, la (re)découverte du plaisir de cuisiner ensemble, la création de zones de convivialité non-marchandes éphémères dans l’espace public, et, bien sûr, la sensibilisation du plus grand nombre au gaspillage alimentaire.Dans une logique open source, chacun est libre d’organiser lui-même sa propre Disco Soupe à la condition que celle-ci respecte nos Discommandements.

Dans le cadre d’un appel à projet satellite de la Triennale de Design Reciprocity, le projet des Disco Soupes à Liège se concrétise peu à peu suite à la sélection dans cette programmation. Une aide financière est apportée de la part des partenaires de l’événement.Cela va permettre de prendre en charge les frais nécessaires au lancement, c’est à dire le petit matériel, la communication ainsi que les différents consommables.

# Mais alors, qu’est-ce qu’on peut faire?

C’est tout simple, nous sommes à la recherche de bénévoles, de volontaires qui seraient prêts à rejoindre l’aventure. 

Plusieurs implications sont possibles, et tout le monde peut s’y retrouver. Nous espérons pouvoir mettre en forme une communauté qui participera à la préparation et l’organisation des événements (environ une Disco Soupe par saison). Cinq, six personnes sont suffisantes pour la mise en place. Par contre le jour-J, plus on est de fous, plus on rit! 

C’est ici, que nous demandons une diffusion, un échange et des partenaires pour faire résonner notre message.
