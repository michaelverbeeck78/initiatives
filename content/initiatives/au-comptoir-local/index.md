---
title: "Au Comptoir Local"
date: 2020-07-08T10:10:58+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Circuit court,Vrac,Boissons,Maraîchage,Produits laitiers,Elevage,Boulangerie,Cosmétiques]
summary: Au Comptoir Local est un point de vente regroupant des produits frais, naturels et de saison, cultivés en agriculture biologique ou raisonnée et provenant (quasi-) exclusivement de producteurs locaux. Chacun de ces produits est soigneusement sélectionné par leurs soins chez des artisans/producteurs soucieux du respect de la nature et de leur mode de production. 
youtubeId: 
valheureux: false
contact: 
    website: "https://aucomptoirlocal.be"
    mail: 
    facebook: aucomptoirlocalbeaufays
    twitter: 
    instagram: 
locations:
  - name: Au Comptoir Local
    address: Place de la Bouxhe 23<br> 4052 Beaufays
    hours: 
    phone: 04 226 02 80
    googlePlaceId: ChIJI5s8gD72wEcReJMwnShds3g
    latitude: 
    longitude: 
---
