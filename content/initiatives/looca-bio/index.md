---
title: "Looca"
date: 2020-07-08T10:09:46+02:00
draft: false
longText: false
categories: [zz-autres]
tags: [Artisanat,Circuit court,Vêtements,Commerce équitable,En ligne]
summary: >
    Vers un max de local pour un look bio et éthique.
youtubeId: 
valheureux: false
contact: 
    website: http://www.looca-bio.be
    mail: contact@looca-bio.be
    facebook: Looca-Biobe-104511377581996 
    twitter: 
    instagram: looca.bio
locations:
  - name: Looca
    address: Rue Maflot 2<br> 4120 Neupré
    hours: 
    phone: 0473/825428
    googlePlaceId: ChIJ62UWFhRWwEcRBkzyGNVrXEE
    latitude: 
    longitude: 
---
Tout a commencé à l’initiative d’une neupréenne de 38 ans, Emeline Detienne, adepte de la seconde main pour habiller ses enfants et en recherche de vêtements bio, éthiques et locaux pour compléter ses trouvailles. Les pièces qu’on perd, qu’on déglingue, qu’on oublie, les sous-vêtements… ont rarement l’occasion de pouvoir bénéficier d’une seconde vie. De plus, on trouve du bio pour les bébés, mais moins facilement pour les plus grands. Et quand on trouve, la confection est rarement locale. Un manque à combler qu’elle a transformé en opportunité !

Emeline a créé la marque Looca : elle gère le choix de fournitures et de leur origine, le design, la vente et coordonne la confection de sa ligne de vêtements. Elle sélectionne des tissus certifiés par des labels reconnus et privilégie les circuits courts, l’ancrage local. Elle met également un point d’honneur à collaborer avec des couturières de la région liégeoise.  Pour vous permettre de connaître l’histoire de chaque pièce, l’origine des éléments qui la composent est détaillée sur le site Internet.

A ses débuts, Looca a commencé par proposer des accessoires d’hiver et d’été (chapeaux de soleil, foulards, tours de cou, bonnets, bandeaux, moufles…) et des petites pièces pour valoriser les chutes de tissu (chouchous). La gamme s’est déjà élargie aux leggings et se prépare pour les slips boxers, les mouchoirs en tissu, les pyjamas, les bavoirs…
Looca, c’est actuellement un e-commerce de proximité : les articles sont disponibles via le site Internet et la livraison « main à main » est gratuite pour les gens du coin. Les petites pièces ont commencé leur entrée dans quelques magasins (2pois2mesures à Nandrin, Au Vert G à Boncelles). Et un petit salon des créateurs locaux se prépare pour début septembre, chez Les Bouchons Liégeois, à Rotheux.  
