---
title: "Pôle créatif liégeois"
date: 2021-08-20T19:44:22+02:00
draft: false
longText: false
categories: ["zz-autres"]
tags: [Blog,Evénèment,Solidarité,Sensibilisation,Education,Culture,Ateliers]
summary: "Le Pôle Créatif Liégeois, association de fait, accompagne bénévolement des porteurs de projets locaux, citoyens ou solidaires."
youtubeId: 
valheureux: false
contact: 
    website: "https://www.polecreatifliegeois.be"
    mail: "contact@polecreatifliegeois.be"
    facebook: "polecreatifliegeois"
    twitter: "polecreatiflieg"
    instagram: "pole_creatif_liegeois"
locations:
  - name: "Pôle créatif liégeois"
    address: "Rue Bois de Mont 174<br>4101 Seraing"
    hours: 
    phone: 
    googlePlaceId:ChIJsxWYG1dal6ARo8d-GItnkSY 
    latitude: 
    longitude: 
---

Plus largement, il favorise aussi la sensibilisation à diverses valeurs qui lui sont chères, comme la solidarité, la créativité, l’inclusion et l’esprit critique.

Au niveau international, il travaille sur une campagne d’inclusion des différences, à l’image du film réalisé par Jodie FOSTER intitulé “Le petit homme”.
