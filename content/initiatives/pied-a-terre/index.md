---
title: "Pied à Terre"
date: 2020-04-29T10:12:39+02:00
categories: ["Alimentation"]
tags: ["Circuit court,Maraîchage,Elevage,Boulangerie,Produits laitiers,Boissons"]
summary: >
    Magasin d'alimentation proposant de consommer mieux : des produits locaux, sains et de saison, issus de l'agriculture 
    biologique ou raisonnée et de l'agroforesterie. Proposer et regrouper sous une seule enseigne, les produits de nos 
    producteurs artisans aux citadins que nous sommes.
youtubeId: ""
contact: 
    website: ""
    mail: "info@piedàterre.be"
    facebook: ""
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Rue Saint-Remy, 13"
    hours: ""
    phone: "04 362 27 78"
    latitude: 50.638814
    longitude: 5.570033
    googlePlaceId: 
---
Ce charmant magasin se situe en plein centre de Liège et a pour but une économie circulaire et un développement durable.