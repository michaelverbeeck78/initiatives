---
title: "Terres Et Natures"
date: 2020-07-08T10:05:17+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Education,Sensibilisation,Evénement,Ateliers]
summary: >
    L’association a pour but de promouvoir une conscientisation de l’importance des enjeux écologiques et environnementaux dans leurs impacts sur le bien-être et la santé en favorisant toute expérience permettant une reconnexion et une meilleure relation entre les hommes et notre foyer premier, la nature. 
youtubeId: 
valheureux: true
contact: 
    website: http://www.terresetnature.be/
    mail: info@terresetnature.be
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: ASBL Terres et Nature
    address: Avenue Neef, 56<br> 4130 Tilff 
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---
L’association se donne également pour but de promouvoir et de s’engager dans toute activité de défense et de protection de l’environnement naturel dans la mesure où toute atteinte au patrimoine naturel et à l’écosystème a pour conséquence une atteinte à notre intégrité, notre santé et bien-être.
Pour ce faire l’association organise des séjours résidentiels dans des environnements naturels, des ateliers découverte, discussions-rencontres, conférences, ateliers créatifs favorisant la prise de conscience par l’utilisation de médias artistiques, etc..


LA NATURE A DES CHOSES À NOUS DIRE....

*"Le Monde ne sera pas détruit par ceux qui font le mal mais par ceux qui les regardent sans rien faire"*
A. Einstein
