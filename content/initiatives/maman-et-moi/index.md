---
title: "Maman Et Moi"
date: 2020-07-08T10:04:58+02:00
draft: false
longText: true
categories: [zero-dechets]
tags: [Seconde main,Vêtements,Puericulture]
summary: >
    Boutique de seconde main : Vêtements dames, vêtements enfants, vêtements de grossesse, chaussures, matériel de puériculture, jeux et jouets. Liste de naissance.
youtubeId: 
valheureux: true
contact: 
    website: http://home.scarlet.be/~mw373466/index.html 
    mail: maman.et.moi.info@gmail.com
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: Maman & Moi
    address: Place Andréa Jadoulle 10<br> 4031 Liège
    hours: 
    phone: 
    googlePlaceId: ChIJRQVv4ED3wEcRZoFdA9Qa6zQ
    latitude: 
    longitude: 
---
Situé à Angleur, ce magasin est littéralement rempli de bonnes affaires. Il suffit de se pencher pour trouver des trésors à des prix très abordables.

Au niveau puériculture, il y a de tout: poussettes, Maxi Cosy, sac de couchage,...
Et les mamans peuvent se faire plaisir aussi en chinant dans le rayon dames.