---
title: "Graines d'Epices"
date: 2020-07-08T10:11:30+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Commerce équitable,Epices,Vrac]
summary: >
    Magasin spécialisé en épices du monde à Liège, pour des épices naturelles et bio issues du commerce équitable. Saveurs et goûts assurés!
youtubeId: 
valheureux: true
contact: 
    website: http://www.grainesdepices.com
    mail: contact@grainesdepices.com
    facebook: grainesdepicesliege
    twitter: 
    instagram: 
locations:
  - name: Graines d'épices
    address: Rue de Serbie 7<br> 4000 Liège
    hours: 
    phone: 
    googlePlaceId: ChIJq-VIB_j5wEcRbOBOFSYwzLc
    latitude: 
    longitude: 
---
