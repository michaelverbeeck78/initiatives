---
title: "Wattitude"
date: 2020-07-08T10:06:17+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Artisanat,Circuit court]
summary: >
    Première vitrine du made in Wallonia, Wattitude propose exclusivement des produits conçus, créés et/ou fabriqués en Wallonie. En plein coeur de la cité ardente, Wattitude est le spot liégeois à haut potentiel tendance et sympathie… un incontournable.
youtubeId: RFt9dsqstmc
valheureux: true
contact: 
    website: https://www.wattitude.be/fr/
    mail: info@wattitude.be
    facebook: Wattitudestore 
    twitter: 
    instagram: 
locations:
  - name: Wattitude
    address: Rue Souverain Pont 7<br> 4000 Liège
    hours: 
    phone: 04 221 44 76
    googlePlaceId: ChIJDfFFkg76wEcRHslrGzOLodI
    latitude: 
    longitude: 
---
Emmanuelle Wégria, en vraie dénicheuse de talents, a sillonné les routes pour dégoter et réunir sous un même toit plus de 250 créateurs, artistes et producteurs de la scène locale. Mode, accessoires, design, mais aussi spécialités gourmandes, découvertes musicales, livres et jouets, Wattitude c’est la promesse d’un style de vie arty, fun et coloré sans pour autant se ruiner.

À travers sa sélection pointue de labels et d’artistes locaux soucieux de l’environnement, Wattitude prouve par A (comme Artistes) plus B (comme Bières) qu’éco-responsabilité et consommation locale ne riment pas forcément avec look de vieux hippie. L’éthique, c’est aussi chic et ludique, qu’il s’agisse de s’habiller, de décorer son appart ou de boire une bière belge entre amis.

Passionnée par les belles matières, les savoirs-faire locaux et l’upcycling, Emmanuelle Wégria entend décliner le mot local bien au-delà des productions dites artisanales, souvent désuètes. Et pourquoi pas faire évoluer les mentalités en matière de consommation plaisir?
