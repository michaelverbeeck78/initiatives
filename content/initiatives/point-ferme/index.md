---
title: "Point Ferme"
date: 2020-07-08T10:05:04+02:00
draft: false
longText: true
categories: [Alimentation]
tags: [Coopérative,Circuit court,En ligne]
summary: >
  PointFerme est une coopérative à finalité sociale regroupant d'abord des producteurs du Condroz liégeois, créée avec le soutien du GAL Pays des Condruses. Ils ont été sélectionnés pour la qualité de leur mode de culture. Ils pratiquent soit une agriculture biologique, soit l'agroforesterie, soit l'agriculture raisonnée.
youtubeId: 
valheureux: false
contact: 
    website: https://www.pointferme.be
    mail: info@pointferme.be
    facebook: pointfermenandrin
    twitter: 
    instagram: 
locations:
  - name: 
    address: Rue Tige des Saules 48<br> 4550 Nandrin
    hours: 
    phone: 085 84 34 60
    googlePlaceId: ChIJ8xmc2ilVwEcR9xnhMYvGQqk
    latitude: 
    longitude: 
---
Maintenant, Point Ferme vole de ses propres ailes, elle est une coopérative ouverte à tous les agriculteurs de la province de Liège. Mais choisit toutefois de ne vous livrer que le meilleur de nos campagnes.Régulièrement, de nouveaux coopérateurs viennent renforcer et augmenter la diversité de produits proposés au magasin de Nandrin ou dans un de ses nombreux points de dépôts. 
