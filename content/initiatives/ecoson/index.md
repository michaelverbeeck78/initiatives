---
title: "Ecoson"
date: 2020-07-08T10:03:38+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Produits ménagers,Vente directe,En ligne,Sensibilisation,Ateliers]
summary: >
    Les produits ménagers de l'entreprise Ecoson, fabriqués artisanalement, nettoient votre maison et votre linge. Respectueux de la nature et préservant votre santé, ils ne sont pas testés sur les animaux et s'intègrent dans le principe du « Zéro déchet ».
youtubeId: 
valheureux: false
contact: 
    website: https://ecoson.net
    mail: ecosonecomine@gmail.com
    facebook: Ecosonensemble
    twitter: 
    instagram: 
locations:
  - name: Ecoson
    address: Route du Vieux Chêne 7a <br> 4190 Werbomont
    hours: 
    phone: 0493 36 01 24
    googlePlaceId: ChIJCeBsrPZdwEcRW6kNxQrMS0g
    latitude: 
    longitude: 
---
Ecoson organise des ateliers thématiques chez vous, en famille, entre amis, voisins ou collègues, pour vous permettre de réaliser vos propres produits tout en vous amusant.

Une idée originale pour animer une babyshower, un enterrement de vie de jeune fille, un goûter d'anniversaire ou simplement passer un moment agréable en apprenant quelques trucs et astuces.
