---
title: "SB Farms"
date: 2020-04-29T10:22:29+02:00
categories: [Alimentation]
longText: true
tags: [Circuit court,Maraîchage,Elevage,Vente en ligne,Produits laitiers,Boissons,Farines,Commerce équitable]
summary: >
    A l’heure de l’industrialisation de l’agriculture, SB Farms entend privilégier un autre mode de consommation : 
    celui de produits sains, issus de l’agriculture locale, cultivés dans des conditions respectueuses de la terre et des 
    hommes.

youtubeId: ""
contact: 
    website: "http://www.sbfarms.be"
    mail: "gregory@sbfarms.be"
    facebook: ""
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Marché Matinal<br>Avenue Joseph Prévers 29<br>4020 Liège"
    hours: ""
    phone: "0484 75 97 97"
    googlePlaceId: ChIJkbbshOfwwEcRf-OyIOUpKS8
---
Le bio fait son entrée sur le marché matinal de Liège grâce à SB Farms: les professionnels de l’alimentation peuvent se fournir en fruits et légumes bio et locaux auprès d’un grossiste sur le marché matinal de Liège. Une première en Belgique !

Service personnalisé pour les collectivités, commerçants, entreprises, professionnels de la restauration.
