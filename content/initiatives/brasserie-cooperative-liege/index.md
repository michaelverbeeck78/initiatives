---
title: "La Brasserie Coopérative Liégeoise"
date: 2020-07-12T10:09:06+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Boissons,Coopérative]
summary: >
    Un pur moment de partage. Un pur projet local et durable. La Brasserie Coopérative Liégeoise est une Coopérative à Finalité Sociale lancée fin 2015, sur le site de la Ferme à l’Arbre de Liège (Lantin), et réunissant plus de 400 coopérateurs.
youtubeId: 
valheureux: false
contact: 
    website: http://www.bcl.bio/ 
    mail: stany@bcl.bio
    facebook: brasseriecooperativeliegeoise
    twitter: 
    instagram: labadjawebio
locations:
  - name: La Brasserie Coopérative Liégeoise
    address: Avenue de l'expansion 4/4<br>4432 Alleur
    hours: 
    phone: 0497 97 57 04
    googlePlaceId: ChIJW3mRnwL7wEcRsVIyo-xNmcc
    latitude: 
    longitude: 
---
Leur objectif est de créer des bières bio en valorisant l’agriculture locale et en utilisant des matières premières issues du circuit court en travaillant main dans la main avec des producteurs locaux et/ou belges, d’autres brasseries et des partenaires de notre région.

De la production à la mise en bouteille, tout est réalisé dans leur nouvelle Brasserie à Alleur.

En plus de brasser avec de l’orge local de la Ferme Schiepers à Wanze et du froment cru de la Ferme à l’Arbre de Liège à Lantin, ils ont leur propre houblonnière qui est installée sur les terres de la ferme afin de couvrir les besoins en houblon de la brasserie tout en continuant à travailler avec Joris Cambie, leur houblonnier installé à Poperinge.

Une Brasserie véritablement locale, bio et solidaire!
