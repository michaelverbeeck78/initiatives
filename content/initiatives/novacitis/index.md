---
title: "Novacitis"
date: 2021-08-20T17:38:04+02:00
draft: false
longText: false
categories: [zz-autres]
tags: [Circuit court,Mobilité douce,Coopérative,Espace de Coworking,Entrepreneuriat de la transition]
summary: "NOVACITIS, coopérative - entreprise sociale, est une coopérative de citoyen.ne.s, d’associations et d’entreprises."
valheureux: false
contact: 
    website: "https://www.novacitis.be" 
    mail: "info@novacitis.be"
    facebook: "novacitis"
    twitter: 
    instagram: "novacitis"
locations:
  - name: "Novacitis"
    address: "Rue de l'Académie 53, 4000 Liège<br>4000 Liège"
    hours: 
    phone: "043398653"
    googlePlaceId: ChIJO6AjKmf7wEcRnSXCIuG-VEo
    latitude: 
    longitude: 
---

Fondée en 2018, NOVACITIS comprend 221 coopérateurs en 2020, dont 41 entreprises.

NOVACITIS a pour ambition de concrétiser la transition vers une économie souhaitable par l’entrepreneuriat coopératif et l’action collective citoyenne. 
NOVACITIS est un moteur collaboratif et coopératif qui mobilise des ressources humaines et financières pour construire un patrimoine commun de biens et de ressources qui bénéficient à tous.

NOVACITIS crée des projets immobiliers coopératifs et développe des entreprises coopératives moteurs de la transition.
