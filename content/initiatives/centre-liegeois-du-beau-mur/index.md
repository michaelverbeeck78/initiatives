---
title: "Centre Liegeois Du Beau Mur"
date: 2020-07-08T10:07:20+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Solidarité,Evènement,Education,Potager commun,Réparation,Sensibilisation, Recyclage]
summary: Créée il y a 30 ans, l’asbl Le Centre Liégeois du Beau-Mur propose des espaces d’échange et d’accueil destinés aux associations et aux citoyens porteurs de projets démocratiques et alternatifs.
youtubeId: 
valheureux: false
contact: 
    website: "https://beaumur.org"
    mail: "info@beaumur.org"
    facebook: "lebeaumur"
    twitter: 
    instagram: 
locations:
  - name: "Centre Liégeois du Beau-Mur" 
    address: "Rue du Beau Mur 48<br> 4030 Liège"
    hours: 
    phone: "04 349 01 44"
    googlePlaceId: "ChIJlWK9vqzwwEcRgA7PL0n-YBA"
    latitude: 
    longitude: 
---
Dans ses actions quotidiennes, le Beau-Mur est soucieux de prendre part à l’évolution de la société ; il participe ainsi activement à la transition citoyenne et s’inscrit aujourd’hui dans un mouvement d’innovation sociale, notamment à travers son rôle d’initiateur du projet Permis de Végétaliser (**Incroyables Comestibles**) à Liège. Celui-ci a pour but de valoriser les espaces cultivables pour le bien commun mais aussi d’unir citoyens et représentants de la Ville afin de faire preuve d’intelligence collective. Par son soutien à ce projet d’envergure international d’agriculture urbaine, le Beau-Mur participe à la construction d’une société nourrie par des valeurs de **solidarité, de coopération, de respect des humains et des écosystèmes**.

Le Centre organise également un **Repair Café**  (Repair Café Grivegnée)

Depuis 10 ans, le Centre Liégeois du Beau-Mur est associé au réseau **« Culture & Développement »** au sein duquel il mène des projets d’éducation permanente dans un objectif d’émancipation citoyenne.
Le Beau-Mur s’est imposé depuis sa création en 1987 comme un acteur incontournable de la vie associative liégeoise. 

Lieu accessible et dynamique, à la croisée de différents réseaux militants, il permet la rencontre entre différents acteurs de la société civile et facilite ainsi l’émergence de nouveaux projets et collectifs.
