---
title: "Ceinture Alimen-Terre Liégeoise"
date: 2020-08-12T10:40:36+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Circuit court,Coopérative,Sensibilisation,Evénèments]
summary: >
    C’est en novembre 2013 que la dynamique de la Ceinture aliment-terre liégeoise (CATL) a été lancée au Manège Fonck, avec la participation de quelques 600 personnes. Lors d’un forum citoyen organisé à cette occasion, les producteurs, citoyens, institutions et toutes les parties prenantes du système alimentaire liégeois ont pu se réunir et réfléchir ensemble à une stratégie de transformation de ce système, dans le sens de sa relocalisation, de sa "décarbonisation" et de sa démocratisation. 
youtubeId: 
valheureux: false
contact: 
    website: https://www.catl.be
    mail: info@catl.be
    facebook: catliegeoise
    twitter: catl_be
    instagram: 
locations:
  - name: CATL
    address: Rue Pierreuse, 23<br> 4000 Liège
    hours: 
    phone: 04 223 15 51
    googlePlaceId: "ChIJS_Cblw36wEcRYSCM_HpJtuA" 
    latitude: 
    longitude: 
---

Depuis début 2019, la Ceinture aliment-terre liégeoise, devenue ASBL, fait officiellement partie du paysage associatif liégeois, et promeut le développement d’un système alimentaire local, durable et équitable pour tous! Au sein de l’écosystème liégeois participant à la transition, nous cherchons avant tout à mobiliser les acteurs de terrain, orienter et mettre en réseau les porteurs de projets, sensibiliser, informer et apporter notre expertise afin d’essaimer les bonnes pratiques. Ces objectifs ont été clairement énoncés et définis dans notre [Charte éthique](https://www.catl.be/charte-de-la-catl/). 

Cette charte se structure autour de six objectifs généraux suivants:

* Atteindre la souveraineté alimentaire
* Redynamiser l’économie liégeoise
* Construire une économie humaine et conviviale
* Inclure le plus grand nombre
* Respecter les écosystèmes
* Construire la démocratie et les partenariats

Ainsi, notre mission peut être définie de la manière suivante: 

***Favoriser le développement de l’alimentation durable et des filières courtes et locales, en sensibilisant à ces thématiques, en soutenant les acteurs qui les composent et en facilitant leur développement.***

---

Afin de mener à bien cette mission générale, nous l’avons déclinée en trois principaux pôles d’activité: 

# Pôle 1 : Intelligence et développement territorial #

La CATL vise à développer sa connaissance des réalités, des besoins, des enjeux et des acteurs de son territoire, en réalisant un travail permanent de diagnostic et de prospective relatif au développement des filières alimentaires sur son territoire. Ainsi, nous sommes en mesure d’identifier et d’analyser les innovations et les bonnes pratiques qui se développent sur notre territoire mais également de recenser les producteurs locaux ainsi que les coopératives de notre réseau afin de vous permettre de mieux consommer, plus facilement. [Diverses cartes](https://www.catl.be/producteurs-2/) par catégorie de produits sont disponibles sur notre site. Vous pouvez notamment retrouver notre carte interactive visant à recenser l’ensemble des points de distribution des coopératives de notre réseau. En consultant [cette carte](https://www.google.com/maps/d/u/1/edit?mid=16dxXcA9OsrwcEZlsBnLBLLfdK13xnK6B&ll=50.55046678571611%2C5.524663749999981&z=10), vous pouvez ainsi localiser les sites les plus proches de chez vous pour vous fournir en produits de qualité. 


# Pôle 2 : Sensibilisation #

La transformation d’un système agroalimentaire local ou régional passe nécessairement par la sensibilisation et la mobilisation de toutes les forces vives du territoire, des mangeurs aux élus en passant par les acteurs intermédiaires. Cela se traduit notamment par la coordination et l'organisation d'événements tels que le Festival Nourrir Liège. En 2020, le [Festival Nourrir Liège](https://nourrirliege.be/) c’est 10 jours de festivités, 96 événements organisés, 101 partenaires, et en 2019, près de 10 000 participants ! Nous avons également produit, en collaboration avec l’asbl Barricade, une petite bible de l’alimentation durable, intitulée Se nourrir autrement à Liège et [téléchargeable sur notre site par ici](https://www.catl.be/wp-content/uploads/2020/03/Se_Nourrir_Autrement_a_Liege_2020_Ed-spe-Nourrir-Liege.pdf). Vous y trouverez tout un tas de bonnes adresses et conseils pour mieux se nourrir à Liège, ainsi que des informations relatives aux modes de productions responsables et de distributions solidaires et de proximité. 



# Pôle 3 : Expertise #

La CATL propose des services et des accompagnements liés à son expertise propre. La CATL offre en particulier des accompagnements pour la mise en place d’un nouveau modèle de cantines scolaires durables dans le cadre du Collectif Développement Cantines Durables. Elle fournit aussi un service de soutien à l’élaboration et à la mise en œuvre de politiques publiques. Enfin, elle est disponible pour complémenter l’offre de services d’acteurs généralistes de l’accompagnement ou de formation, comme par exemple notre collaboration avec Step Entreprendre, dans le cadre d’une formation dédiée à l’accompagnement spécifique au secteur alimentaire durable. Si vous souhaitez entreprendre dans l’alimentation durable et les circuits-courts, vous pouvez retrouver notre offre de formation par ici. Si vous désirez vous lancer et développer votre projet dans l’agro-alimentaire nous pouvons vous y aider et vous accompagner. 


Pour résumer, à la CATL, nous nous employons à faciliter l’accès pour chaque habitant de l'Arrondissement de Liège à une alimentation saine et locale via notamment le soutien au développement d'un réseau de distribution éthique et coopératif. 

Nous sommes convaincus que cela passe nécessairement par de la sensibilisation afin de partager les bienfaits et les nombreux intérêts qui existent à se nourrir sainement et localement, avec une attention particulière pour les jeunes générations. Ainsi, nous contribuons par exemple via notre action au sein du Collectif Développement Cantines à faire basculer des dizaines d’écoles (60 actuellement en Wallonie, potentiellement plusieurs centaines à un horizon de quatre à cinq ans) vers un nouveau modèle de cantine durable. 

Mais nous nous engageons également pour la promotion de la démocratie alimentaire, élément évidemment essentiel si nous voulons engager un maximum d’acteurs et de citoyens dans l’aventure de l’alimentation durable. Dans cette optique-là, nous souhaitons vivement et soutiendrons la création d'un Conseil de Politique Alimentaire pour l'Arrondissement de Liège. 

Et enfin, de tester et d'essaimer des innovations sociales permettant une amélioration sensible du revenu et de la qualité de vie des producteurs. La création de l’asbl [Les Pousses Poussent](https://www.facebook.com/LesPoussesPoussent/), en collaboration avec la Ville de Liège (via le projet CREaFARM) et la coopérative [Les Petits Producteurs](https://lespetitsproducteurs.be/) en est un très bon exemple. Ainsi, rue du Plope à Liège, les deux jeunes maraîchers installés sur la parcelle « CREaFARM » mise à disposition par la Ville, bénéficient de meilleures conditions socio-économiques, notamment grâce à un statut hybride innovant, des prix garantis sur l’année et un système de vente avec abonnements en auto-cueillette. Pour plus d’informations à propos de ce projet, vous pouvez lire [notre dernier article](https://www.catl.be/2020/07/07/lumiere-sur-les-pousses-poussent/). 

La Ceinture aliment-terre liégeoise s’inscrit dans le mouvement des initiatives citoyennes qui participent activement à la transition de la ville de Liège vers toujours plus d’alimentation durable, locale et vers un système agro-alimentaire solidaire, respectueux des écosystèmes et résilient. 

Pour garder le contact et être tenu informé des actualités inspirantes de la région, et des futurs projets de la CATL, n’hésitez pas à nous retrouver sur notre page Facebook, sur twitter ou encore sur notre site internet, où vous pourrez renseigner votre mail afin de recevoir notre newsletter mensuelle. 
