---
title: "Equifrais"
date: 2020-07-30T10:09:12+02:00
draft: false
longText: true
categories: [Alimentation]
tags: [Circuit-court,En ligne]
summary: >
    En juin 2017, Marc Bellefroid décide de changer complètement d’orientation pour se recentrer sur sa région et surtout sur différents concepts qui lui tiennent à cœur : bien manger, manger équilibré, manger sainement, manger local, manger « circuit court », manger éco-responsable. 
youtubeId: 
valheureux: false
contact: 
    website: https://www.equifrais.be
    mail: 	info@equifrais.be
    facebook: Equifrais
    twitter: equifrais
    instagram: 
locations:
  - name: 
    address: Liège
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---
Commence alors une aventure extraordinaire pour monter ce projet de A à Z en alliant la modernité de l’e-commerce (pour la commande des clients), le professionnalisme indispensable à ce métier (notamment pour garantir complètement la chaîne du froid) et le côté rural et sympathique des producteurs locaux et des coopératives rencontrées ci et là.

La philosophie d'Equifrais est de procurer à ses clients des produits (locaux) de qualité, de favoriser le circuit court et de permettre à ceux qui lui font confiance de manger équilibré sans devoir aller faire de nombreuses courses.
En livrant les justes quantités pour vos repas, Equifrais lutte ainsi significativement au gaspillage.

Sur le site internet, les recettes proposées sont préparées par des chefs liégeois pour des Liégeois.
Ces recettes sont revues (et corrigées) en grande partie par un médecin nutritionniste afin que les repas soient aussi équilibrés possible.

Les boîtes contenant les recettes et les différents ingrédients sont livrées en camionnettes frigorifiques dans le plus grand respect de la chaîne du froid.
