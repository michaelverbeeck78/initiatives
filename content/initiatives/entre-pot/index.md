---
title: "L'Entre-Pot"
date: 2020-04-29T09:30:01+02:00
categories: [zero-dechets]
tags: [Boissons,Farine,Epices,Produits ménagers,Cosmétiques,Vrac,Vente directe,Circuit court,Ateliers]
summary: L’Entre-Pot est le magasin spécialiste du zéro déchet près de la place du Marché où l’on trouve des denrées alimentaires en vrac (féculents, épices, huiles, chocolat,…) mais aussi un coin cosmétiques bien fourni (shampoings et savons solides, têtes de brosses à dents recyclables, cotons tiges ré-utilisables, …). Il y a aussi les bocaux Lili Bulk qui nous facilitent la vie, ainsi que de quoi prendre l’apéro et du vrac pour laver la maison et le linge. Pour celles et eux qui manquent de temps, il est même possible de passer une pré-commande (via le site internet). Tout est si bien organisé qu’on a envie de tout tester.
youtubeId: 
contact:
    website: "http://www.lentre-pot.be"
    mail: coucou@lentre-pot.be
    facebook: "LEntrePotLiege"
    twitter: ""
    instagram: l_entre_pot
locations:
  - name:
    address: Rue du Palais 6<br> 4000 Liège
    hours: 
    phone: "04 223 26 06"
    latitude: 50.647641
    longitude: 5.575701
    googlePlaceId: "ChIJWd9uRAz6wEcRQpUneba8XLw"
---
