---
title: "Au Fond Des Pans"
date: 2021-08-20T17:04:17+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Maraichage,Elevage,Vente directe,Circuit court,Commerce équitable,Monnaie locale,Apiculture,Pépinière]
summary: "Production de légumes, viande et œufs sur une petite ferme biologique, cultivés dans le village de Deigné."
youtubeId: 
valheureux: false
contact: 
    website: "https://www.aufonddespans.be"
    mail: "info@aufonddespans.be"
    facebook: "fonddespans"
    twitter: 
    instagram: "fonddespans"
locations:
  - name: Au fond des Pans
    address: Deigné 93A<br>4920 Aywaille
    hours: 
    phone: "0493 67 47 17"
    googlePlaceId: ChIJqRAMfrhfwEcRmr70pZrRb24
    latitude: 
    longitude: 
---

Les producteurs d’**Au fond des pans** travaillent dans le plus grand respect de la terre et de l'environnement, sans pesticide et sans engrais chimique.
Leurs fruits et légumes sont disponibles au détail directement dans leur magasin ou en livraison à domicile.
