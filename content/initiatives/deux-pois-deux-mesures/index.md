---
title: "Deux Pois Deux Mesures"
date: 2020-07-08T10:08:37+02:00
draft: false
longText: true
categories: [Alimentation]
tags: [Produits laitiers,Boissons,Farine,Epices,Produits ménagers,Cosmétiques,Vrac,Circuit court,Monnaie locale,Coopérative,Ateliers]
summary: >
    Située au bord de la route du Condroz, à Nandrin, "Deux pois, deux mesures" est une épicerie conviviale qui propose la grande majorité de ses produits en vrac.
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: info@deuxpois-deuxmesures.com
    facebook: Deux-pois-deux-mesures-135165573724081
    twitter: 
    instagram: 
locations:
  - name: Deux Pois Deux Mesures ( DPDM )
    address: Route du Condroz 123b<br> 4550 Nandrin
    hours: 
    phone: 085 25 16 26
    googlePlaceId: ChIJzXMsJPxUwEcR-hFA_8DIqHs
    latitude: 
    longitude: 
---
Des épices aux pâtes, de l'acide citrique à au savon solide, du yaourt de la région au fromage du coin, chaque produit est sélectionné avec attention pour répondre au minimum à 2 des 3 critères suivants : vrac, bio, local.

Il est aussi possible d'y trouver de quoi équiper sa maison dans une optique durable et zéro déchets et être conseillé sur sa transition quotidienne.

La particularité de l'endroit est d'être complet et de proposer tout ce dont on a besoin pour éviter les supermarchés, en privilégiant des petits producteurs.
En bref, pas sur suremballage, pas d'étiquettes m'as-tu-vu, mais du bon, du belge, du bio.

