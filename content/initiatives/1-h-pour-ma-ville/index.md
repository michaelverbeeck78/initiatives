---
title: "1h pour ma ville"
date: 2020-07-08T10:00:48+02:00
draft: false
longText: false
categories: [Zero-Dechets]
tags: [Education,Sensibilisation,Evènements,Solidarité]
summary: 1h pour ma ville est un mouvement de citoyens volontaires qui se réunissent un samedi ou un dimanche par mois de 11h à 12h, juste 1h pour ramasser des déchets et créer ainsi un effet de masse simultanément. 
youtubeId: 
valheureux: false
contact: 
    website: https://www.facebook.com/groups/561022977672111/
    mail: julian.huls@yahoo.fr
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: 
    address: Liège
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---

1h en théorie car certains events pourraient nous encourager à prendre 2 à 4 h de temps pour un special event par exemple. 

Si toi aussi, tu as envie de faire quelque chose pour Liège, ses rues, ses parcs, ses forêts, viens nous rejoindre pour le grand mouvement de masse de **#1hpourmaville**... 

Ensemble, nous pouvons changer les choses....
