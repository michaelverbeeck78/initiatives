---
title: "Incroyables Comestibles"
date: 2020-07-08T10:04:00+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Potager commun, Solidarité]
summary: >
  Le projet « Cultivons Liège, Ville comestible » fait de l’espace urbain liégeois un espace comestible à partager. Grâce au permis de végétaliser, il crée une ville où l’autonomie alimentaire passe par l’action citoyenne dans l’espace public selon le concept des « Incroyables Comestibles », né en Angleterre.
youtubeId: 
valheureux: false
contact: 
    website: http://www.permisdevegetaliser.be
    mail: animation@beaumur.org
    facebook: Incroyables-Comestibles-Li%C3%A8ge-355546624549568
    twitter: 
    instagram: 
locations:
  - name: Liège
    address: Rue du Beau-Mur, 48<br> 4030 Liège
    hours: 
    phone: 04/349 01 44
    googlePlaceId: ChIJlWK9vqzwwEcRgA7PL0n-YBA
    latitude: 
    longitude: 
---
Soutenez les liégeois dans leur transition vers une agriculture urbaine durable et construisons ensemble la Ville que nous voulons plus verte, écologique et solidaire.

Concrètement, des espaces potagers sont installés dans l’espace public par des citoyens qui en font la demande.
