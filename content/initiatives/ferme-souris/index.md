---
title: "Ferme Souris"
date: 2020-08-12T11:19:10+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Maraîchage,En ligne,Vente directe,Circuit court,Evènement,Solidarité,Sensibilisation,Education,Ateliers]
summary: >
    Des légumes produits avec que de l’amour, du fumier et de l’huile de coude! Ils cultivent une petite surface de maraîchage diversifié en veillant à la santé de tous.
    Panier surprise, vente en ligne, achat sur place un mercredi sur deux du 15 mai au 15 janvier … Toutes les facilités sont là pour profiter de leurs bons fruits et légumes.

youtubeId: 
valheureux: false
contact: 
    website: 
    mail: info@fermesouris.be
    facebook: fermesouris 
    twitter: 
    instagram: 
locations:
  - name: La Ferme Souris
    address: Rue de Villers 5<br> 4432 Xhendremael	
    hours: 
    phone: 
    googlePlaceId: "ChIJH7o-N5j9wEcRCXcDaCLo8gg"
    latitude: 
    longitude: 
---
