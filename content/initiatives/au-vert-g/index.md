---
title: Au Vert G
date: 2020-04-29T09:30:24+02:00
categories: [Alimentation]
tags: [Circuit court,Produits laitiers,Produits ménagers,Cosmétiques,Elevage,Maraîchage,Boulangerie,Boissons,Vrac]
summary: Magasin spécialisé dans les produits biologiques de toutes sortes avec un nouvel espace vrac bien fourni, une commande de paniers légumes disponibles, un espace cosmétique, des produits d’entretien zéro déchets, du pain, une belle fromagerie… Vous y trouverez très probablement ce que vous cherchez!
youtubeId:
contact: 
    website: https://www.vertg.be/
    mail: contact@vertg.be
    facebook: auvertg
    twitter:
    instagram:
locations:
  - name: Au vert G
    address: Rue de l'Eglise 50, 4100 Seraing
    hours: >
      mardi - mercredi - vendredi: 09:30 - 18:30 <br>
      jeudi: 09:30 - 20:00 <br>
      samedi: 09:30 - 18:00 <br>
      dimanche: 09:30 - 13:00
    phone: "04 337 01 93  "
    googlePlaceId: "ChIJcdj4h0P4wEcRWckbsO46KHI"
    latitude: 50.577378
    longitude: 5.535924
---
