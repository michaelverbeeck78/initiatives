---
title: "Goupille Mobile"
date: 2020-07-08T10:07:43+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Circuit court,Seconde main,Réparation,Mobilité douce,Espace de Coworking,Solidarité]
summary: >
    "Goupille Mobile" est une activité créée par Fanny Michel qui est une artisane qui propose des services de dépannages et de réparations de serrure. Elle travaille en se déplaçant en vélo avec sa remorque à outils dans Liège en privilégiant la revalorisation de matériaux d'occasion si possible, et essayant de s’ajuster dans la mesure du possible au budget de ses client.e.s.
youtubeId: 
valheureux: true
contact: 
    website: https://www.comptoirdesressourcescreatives.be/les-pages-pros/membre/goupille-mobile-469
    mail: goupille.mobile@zaclys.net
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: Ateliers Mutualités Dony
    address: Rue Dony 33<br> 4000 Liège
    hours: 
    phone: 0497/708544
    googlePlaceId: ChIJdRdU75jwwEcRoCHjG3FP3i8
    latitude: 
    longitude: 
---
Fanny Michel crée aussi des objets adaptés à la vie quotidienne de personnes porteuses de handicap moteurs et/ou mentaux (Fanny est logopède de formation, et cette activité lui permet de fusionner son métier de mécanicienne et de thérapeute).

Elle possède un atelier dans l'espace Dony (quartier saint léonard), géré par le Comptoir des Ressources Créatives. https://www.comptoirdesressourcescreatives.be/les-pages-pros/membre/goupille-mobile-469
Fanny réalise aussi des réparations en tout genre de type mécanique, sans électricité (préférences pour les systèmes et machines low-tech qu’elle conçoit et/ou répare). 

Elle est aussi plasticienne et bijoutière autodidacte. https://www.comptoirdesressourcescreatives.be/les-pages-pros/membre/fenouil-470
