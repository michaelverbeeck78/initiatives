---
title: "Goutte à Gotte"
date: 2020-07-08T10:08:07+02:00
draft: false
longText: false
categories: [zz-autres]
tags: [Sensibilisation,Education,Potagers communs,Ateliers]
summary:
youtubeId: 
valheureux: false
contact: 
    website: https://www.goutteagotte.com
    mail: goutteagotte@gmail.com
    facebook: Goutte-%C3%A0-Gotte-1682247528672423
    twitter: 
    instagram: 
locations:
  - name: Goutte à Gotte
    address: Insegotte 3<br> 4181 Hamoir
    hours: 
    phone: 086/840 664 - Aline Sauvage 0474/ 92 84 12
    googlePlaceId: EiFJbnNlZ290dGUgMywgNDE4MSBIYW1vaXIsIEJlbGdpdW0iUBJOCjQKMglp6tEwUlrARxE3bot3rmQJBBoeCxDuwe6hARoUChIJdaTYmFZawEcRDVlrgeae6zUMEAMqFAoSCfNmdXtRWsBHEfgs00g5bLyJ
    latitude: 
    longitude: 
---
En septembre 2012 est née l’asbl « Goutte à Gotte ». Ce nom  a été choisi parce qu’il reflète d’une part la patience dans un monde où tout va toujours plus vite et d’autre part le lien avec la terre et son histoire (« Inse-Gotte »).

Le hameau d'Insegotte se situe à la frontière entre l'Ardenne, le Condroz et la Famenne (province de Liège, région wallonne). Inclus dans la commune de Hamoir, il offre un havre de dépaysement proche de lieux touristiques (Durbuy, Huy, Liège, Remouchamps, Spa).

Subsidiée par la Région wallonne depuis 2014, l'asbl propose des animations scolaires et activités (ateliers pour enfants et adultes ainsi que des stages pour enfants) qui témoignent de leur désir de revenir à un mode de vie et de consommation plus proche de la nature. L'équipe veut , à travers ce projet, s'impliquer concrètement et à son échelle dans le défi environnemental que nous connaissons.

En 2016, l'équipe s'est agrandie grâce au soutien de la Région wallonne.
