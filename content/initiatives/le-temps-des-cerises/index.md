---
title: "Le Temps Des Cerises"
date: 2020-07-08T10:04:51+02:00
draft: false
longText: true
categories: [Alimentation]
tags: [Maraîchage,Elevage,Produits laitiers,Boissons,Farine,Boulangerie,Epices,Produits ménagers,Cosmétiques,Vêtements,Vrac,Vegan,Circuit court,Commerce équitable,Monnaie locale,Coopérative,Solidarité,Sensibilisation,Ateliers,Herboristerie]
summary: >
    Une grande épicerie biologique et naturelle de quartier...
    Un lieu vivant, chaleureux et familial....
    Au cœur d’un quartier urbain mais vert, le Laveu à Liège....
    Un magasin où l’on trouve tout pour vivre bio au quotidien...
    Une sélection soignée, des conseils et du sourire!
youtubeId: 
valheureux: true
contact: 
    website: http://www.tdcerises.be
    mail: letempsdescerises@outlook.be
    facebook: LeTempsDesCerisesSc
    twitter: 
    instagram: 
locations:
  - name: Le Temps des Cerises, Coopérative
    address: Rue du Laveu 20<br> 4000 Liège
    hours: 
    phone: 04 252 07 00
    googlePlaceId: ChIJ5z3-vR_6wEcRr5KqzTNZLSk
    latitude: 
    longitude: 
---
C’est surtout ...
- une coopérative de clients de consomm’acteurs engagés depuis 1987
- une assemblée générale démocratique,
- l’aventure d’une équipe de travailleurs en chemin vers la transition...
Bref, une grande histoire humaine... qui défend le bio sur tous les fronts!

Mais le Temps des Cerises, c’est aussi...
Un endroit où la magie se faufile discrètement...
Un beau jardin, un formidable terreau pour croire en ses rêves!!!!
Un projet humaniste qui offre des emplois égalitaires....
Une caverne  loin d'Alibaba où les mille trésors se payent aussi en Val'heureux... 


