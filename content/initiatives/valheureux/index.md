---
title: "Le Val'heureux"
date: 2020-08-12T11:18:00+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Commerce équitable,Monnaie locale,Sensibilisation,Education]
summary: >
    Le Val'heureux est une monnaie citoyenne lancée en 2014. Elle soutient l'économie réelle, locale et éthique, les petits producteurs et les petits commerçants en province de Liège.
youtubeId: 
valheureux: false
contact: 
    website: https://valheureux.be
    mail: info@valheureux.be
    facebook: valeureux
    twitter: 
    instagram: valheureuxliege
locations:
  - name: Le Val'heureux
    address: Rue Pierreuse, 57<br> 4000 Liège 
    hours: 
    phone: 
    googlePlaceId: ChIJ40nfeQ36wEcRouiavGEMfqg
    latitude: 
    longitude: 
---
Ce "bon de soutien à l’économie locale" s’utilise en parallèle de l’euro pour stimuler la transition vers une économie plus respectueuse de l’être humain et de l’environnement.

Il circule dans le bassin économique de la région liégeoise : entre Huy et Verviers, en Hesbaye, Condroz, Ourthe-Amblève et dans le pays de Herve.

Le Val’heureux est une initiative de citoyens regroupés au sein de l’ASBL du même nom.

Plus de 300 commerçants et producteurs l'acceptent !

Les objectifs du Val’heureux :
• renforcer et valoriser l’économie locale et les circuits courts ;
• favoriser l’utilisation de biens et de services socialement responsables ;
• soutenir les initiatives respectueuses de l’environnement ;
• promouvoir en particulier la souveraineté alimentaire et économique ;
• créer du lien social sur base locale tout en facilitant les échanges.

Utiliser le Val’heureux, c’est soutenir les petits entrepreneurs, retenir et mieux faire circuler la richesse dans l’économie régionale.

La monnaie citoyenne aide aussi à mieux comprendre et agir collectivement sur notre économie du quotidien.
