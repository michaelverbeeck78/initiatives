---
title: "Maison Zero"
date: 2020-08-12T11:13:03+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Produits ménagers,Cosmétiques,Circuit court,Commerce équitable,En ligne]
summary: >
    Maison Zéro est une site internet qui propose des kits thématiques composés de produits locaux, les quelques essentiels pour vous initier à un mode de consommation plus responsable et garantir un avenir plus juste, plus durable et moins plastique!  

youtubeId: 
valheureux: false
contact: 
    website: http://www.maisonzero.be
    mail: maisonzero.be@gmail.com
    facebook: maisonzerobe
    twitter: 
    instagram: maison.zero
locations:
  - name: 
    address: 
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---
Chaque kit est composé de 3 à 6 produits créés par des artisans locaux pour vous aider à réduire vos déchets; des fiches didactiques sur les produits proposés; ainsi qu’un sac à vrac en coton bio réutilisable, pour éviter le packaging à usage unique et vous permettre de faire vos courses.

Les kits sont liés à une pièce de la maison, un événement ou une thématique précise et sont à acheter de manière ponctuelle, sans formule d’abonnement. Vous êtes déjà actif dans une démarche de consommation éco-responsable? Il est également possible d'acheter les produits disponibles en version individuelle!
