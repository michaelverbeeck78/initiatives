---
title: "Vent De Terre"
date: 2020-07-08T10:05:23+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Maraîchage,Circuit court,Education,Vente directe,Boulangerie]
summary: >
    Projet de maraîchage diversifié en agroécologie, avec une grande diversité de légumes cultivés (plus de 80 variétés). Vent de Terre cultive sur deux sites. 
youtubeId: 
valheureux: false
contact: 
    website: https://www.ventdeterre.be/ 
    mail: info@ventdeterre.be
    facebook: vdterre
    twitter: 
    instagram: 
locations:
  - name: Vent de Terre
    address: Rue d'Angleur 92<br> 4130 Esneux
    hours: 
    phone: 0470 58 16 02
    googlePlaceId: ChIJRbpvcr33wEcRWAnYBRYBzS0
    latitude: 
    longitude: 
---
Le premier site est partagé avec Écotopia sur l'ancien site des pépinières Gallet, près de Tilff, où leur surface cultivée atteint 60 ares (6.000 mètres carrés) avec des plantations de nombreux petits fruits (cassis, framboises, groseilles, mures, myrtilles…), d’une haie indigène autour de la parcelle principale et l’introduction de fleurs pour la biodiversité de même que plus de 150 arbres fruitiers moyennes-tiges. Les second site se situe à Mehagne près du Carmel et est en développement depuis 2018. Sur celui-ci où à terme 60 ares de culture y seront installés et un grand nombre d'arbre fruitier y ont été plantés le jardin pédagogique qui accueille des enfants de 3à 12 ans depuis juillet 2020. Leurs méthodes de culture sont celles de l’agroécologie, sans labour, avec des semences bios, une bonne rotation des cultures, l’association des légumes, pas d’intrants chimique,.. tout en s'inspirant des principes de la permaculture.
De plus deux membre de l’équipe réalisent un délicieux pain artisanal chacun une fois par semaine à l’Abbaye de Brialmont.
Vent de Terre à également fait l’acquisition d’une presse à fruit à la ferme de Targnon où vous pouvez y amener vos pommes et poires pour les faire transformer en délicieux jus.

# Leurs valeurs # 
Leur raison d’être, c’est d’incarner le changement qu’ils veulent voir dans le monde. Ils ont bâti leur projet sur 3 valeurs : RESPECT – AMOUR et HUMILITÉ. Ils travaillent aussi à la gouvernance humaine: c’est-à-dire que chacun a droit à la parole et ils prennent chaque décision de manière concertée.

# Que faire? #
Et bien prendre soin de la planète, au quotidien, tout en prenant soin des Hommes. Manger mieux, pour notre corps, pour la planète… Vivre autrement aussi, dans les petits gestes du quotidien, se rendre compte que l’eau potable est précieuse, que consommer responsable et éthique peut faire changer les choses. C’est pour cela qu'ils ont créé Vent de Terre, et qu'ils vont continuer à vivre à fond cette aventure. Parce qu’il est de notre responsabilité de transmettre un monde vivable à nos enfants, parce que nous n’avons pas le droit de le leur voler pour satisfaire à nos propres soi-disant besoins qui ne sont souvent que des envies créées de toutes pièces par la société de consommation.


