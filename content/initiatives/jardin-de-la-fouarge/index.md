---
title: "Jardin de la Fouarge"
date: 2020-02-25T22:56:54+01:00
categories: ["a_producteur"]
tags: ["Maraîchage,Vente Directe"]
summary: >
    Production locale de légumes frais et naturels cultivés dans le village d’Oneux, sur les hauteurs de Comblain-au-Pont.
    Tout le travail se fait dans le respect de la terre, sans pesticides et sans engrais chimique, avec l'aide de Pétula, un cheval de trait ardennais.
    Au fil des saisons, ils vous proposent en vrac ou en panier de nombreux légumes, petits fruits et autres saveurs locales.
youtubeId: ""
contact: 
    website: "https://www.jardindelafouarge.com/"
    mail: "sarah.remy@hotmail.be"
    facebook: "lafouarge"
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Rue de la Fouarge, 4170 Comblain-au-Pont "
    hours: ""
    phone: "0495 56 75 17"
    latitude: 
    longitude: 
    googlePlaceId: "ChIJFw3Zn51ZwEcRMNPg2kgap5c"
---
