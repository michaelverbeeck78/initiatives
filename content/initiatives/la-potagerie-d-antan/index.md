---
title: "La Potagerie D'Antan"
date: 2020-04-29T10:54:58+02:00
categories: ["a_producteur"]
tags: [Maraîchage,Vente directe,Education]
longText: true
summary: La Potagerie d’Antan est une reconversion naturelle d’un potager familial  sur les hauteurs de Theux.  Petit à petit, grâce au bouche à oreille, le potager s’agrandit, pour arriver à cultiver davantage de variétés de légumes. Le maraîchage est fait à la main, sans aucune mécanisation à la méthode de nos ancêtres.
youtubeId:
contact: 
    website: "https://www.lapotageriedantan.be/ "
    mail: "info@lapotageriedantan.be"
    facebook: "LaPotageriedAntan"
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Grand'Place-lez-Tancrémont, Theux"
    hours: ""
    phone: "0478 95 99 75"
    googlePlaceId: 
    latitude: 50.545463
    longitude: 5.779606
---
**Le maraîchage se veut 100% naturel.** Pas d’utilisation d’engrais ni pesticide, aucun produit, même pas de produits bio.
    
Tout est fait manuellement, à l’ancienne : préparation du sol, plantation, récolte, désherbage,… Il faut laisser le temps à la nature de faire les choses. C’est comme cela que nous pouvons retrouver le bon vrai goût du légume.
Culture de légumes anciens (topinambours, panais, racine de persil,…). Produit phare : la tomate, avec plus de 1100 plants (+/- 50 variétés).

Le potager est également ouvert aux écoles et autres institutions. Une basse-cour de complaisance avec poules, canards, lapins, moutons et émeus anime le domaine au plus grand plaisir des enfants.
Ils organisent des journées découvertes, des stages, des visites et des ateliers.

Des espaces de cultures spécifiques sont également prévus.