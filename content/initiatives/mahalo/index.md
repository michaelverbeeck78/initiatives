---
title: "Mahalo"
date: 2020-07-08T10:06:20+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Cosmétiques,Commerce équitable,Vrac,Vegan]
summary: >
    MAHALO, c'est une équipe experte et passionnée qui propose, depuis 2016, une sélection pointue de produits de beauté et de soins bio.
youtubeId: 
valheureux: false
contact: 
    website: https://www.mahalo.be
    mail: info@mahalo.be
    facebook: mahalobeautystore
    twitter: 
    instagram: 
locations:
  - name: Mahalo Concept Store
    address:  Rue de l'Université 35<br> 4000 Liège
    hours: 
    phone: 0499/111079
    googlePlaceId: ChIJc72bCA_6wEcRekuYrZ_R5Pw
    latitude: 
    longitude: 
---
Cette équipe souhaite apporter une alternative durable et efficace à l’industrie cosmétique classique: plus propre, plus respectueuse et plus réfléchie.

Dans un marché qui évolue rapidement, elle travaille au quotidien à analyser et tester pour vous les différents produits disponibles sur le marché afin de sélectionner les meilleurs.

L’équipe souhaite faire en sorte que les produits naturels ne soient plus un luxe ni un compromis sur l'efficacité.
Avec un assortiment large qui couvre les soins du Visage, du Corps, des Cheveux, Maquillage, Aromathérapie, Parfums, elle a l'ambition de devenir la référence en matière de Beauté et Soins Bio.

# Engagements #

**Une SÉLECTION:** Nos marques sont sélectionnées avec le plus grand soin en analysant scrupuleusement leur éthique, les différents ingrédients, le processus de fabrication, l’impact environnemental, etc.

**Une TOLÉRANCE ZÉRO:** 
Aucun de nos produits n'est testé sur les animaux et nous ne tolérons aucun ingrédient nuisible tout en gardant le focus sur l’efficacité, sur laquelle nous ne faisons aucun compromis. 

**Un SERVICE:** 
Nous nous engageons également à vous offrir le meilleur service et à vous conseiller de la manière la plus personnalisée possible, le tout dans un esprit positif et avec toute la passion qui nous anime!
