---
title: "La Cité s'invente"
date: 2020-08-12T11:21:59+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Culture,Education,Stage,Ateliers,Maraichage,Sensibilisation,Solidarité,Evènements]
summary: >
    Centre d'Initiatives pour une Transition Écologique, L’ASBL est une invitation à l'action collective! L'équipe propose ateliers, chantiers et formations sur 4 thématiques: l'habitat, l'énergie, l'alimentation et la biodiversité. Créée en 2006 par un groupe de jeunes issus des secteurs de l’environnement et de la culture, La CITE s’invente est une ASBL d’Éducation relative à l’Environnement. 

youtubeId: 
valheureux: false
contact: 
    website: http://www.lacitesinvente.be/
    mail: info@lacitesinvente.be
    facebook: pagelacitesinvente
    twitter: 
    instagram: lacitesinvente
locations:
  - name: La Cité s'invente
    address: Rue du Bâneux 75<br>4000 Liège 
    hours: 
    phone: 
    googlePlaceId: ChIJXd8gy5_wwEcRrWZMQsOOW74
    latitude: 
    longitude: 
---
Le principal projet de l’association est la création d’un écocentre à Liège, dans les coteaux de la Citadelle. Un écocentre est un lieu de sensibilisation, de formation et de démonstration pédagogique qui expérimente et explique des techniques et pratiques respectueuses de l’environnement. Leur envie de départ, suscitée par la découverte d’éco-centres à travers le monde, a été de faire exister un lieu de découvertes, d’expérimentations et de réflexions ayant trait aux défis environnementaux. 

Leur intention fut de l’implanter dans un tissu urbain multiculturel pour questionner, par la pratique, les alternatives écologiques pour tous. Ils aménagent depuis 2009 un site qui présente de manière didactique une large palette d’alternatives et de techniques ayant trait aux quatre thématiques que nous développons : l’énergie, l’habitat, la biodiversité et l’alimentation. Leurs missions sont, d’une part, l’information et la sensibilisation liées aux défis environnementaux, et, d’autre part, la promotion des alternatives respectueuses de l’environnement. Ce grand chantier constitue également une occasion unique de pouvoir multiplier les partenariats et les rencontres pour refléter et valoriser les savoir-faire de notre région.
