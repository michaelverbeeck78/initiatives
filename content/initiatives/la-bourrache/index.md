---
title: "La Bourrache"
date: 2020-04-29T11:15:58+02:00
categories: [a_producteur]
longText: true
tags: [Maraîchage,Vente directe,Services,Circuit court]
summary: "La Bourrache est une entreprise d’économie sociale qui propose deux types de services: la vente de légumes bios et de saison, ainsi que l’aménagement et l’entretien de jardins, le tout dans un esprit écologique."
youtubeId: 
contact:
    website: "https://www.labourrache.org"
    mail: "info@labourrache.org"
    facebook: "labourracheASBL"
    twitter: ""
    instagram: "la_bourrache"
locations:
  - name: "La Bourrache ASBL (Bureaux)"
    address: "Rue Chaussée 46, 4342 Awans"
    hours: ""
    phone: "0476 94 11 10"
    latitude: 50.687587
    longitude: 5.462982
    googlePlaceId: "ChIJlWK9vqzwwEcRq-bMCGvW6oo"
  - name: "Adresse du terrain"
    address: "Rue de Looz, 4432 Xhendremael"
    hours: ""
    phone: ""
    latitude: 
    longitude: 
    googlePlaceId: 
---
Ecologique non seulement parce que leurs techniques de travail sont respectueuses de l’environnement, mais également parce que leurs produits sont bien évidemment locaux. Le producteur et le consommateur ne sont séparés ni par des milliers de kilomètres, ni par des intermédiaires aux pratiques parfois contestables.
 
La Bourrache est également et surtout un Centre d'Insertion Socioprofessionnelle (CISP), anciennement Entreprise de Formation par le Travail (EFT) agréée par la Région Wallonne.
L’objectif principal de la formation est de permettre à des demandeurs d’emploi de longue durée ou à des personnes ayant un faible niveau de qualification de reprendre pied dans le marché de l’emploi, non seulement par l’acquisition de compétences techniques, mais également de compétences sociales. Les compétences acquises peuvent être utiles pour intégrer une formation plus poussée par la suite ou pour trouver un travail dans le domaine du jardinage et du maraîchage.
