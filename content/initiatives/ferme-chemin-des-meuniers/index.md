---
title: "La ferme du chemin des Meuniers"
date: 2020-07-08T10:11:17+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Elevage,Produits laitiers,Vente directe]
summary: >
    Daniel et Suzanne gèrent une exploitation laitière en polyculture-élevage, le tout en agriculture biologique.

youtubeId: ExAFgHxIfXA
valheureux: false
contact: 
    website: http://www.coquettesauxpres.be
    mail: info@coquettesauxpres.be
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: Coquettes aux pres
    address: Chemin des Meuniers<br> 4140 Sprimont
    hours: 
    phone: 0495 33 61 01
    googlePlaceId: ChIJ4_vI9LBfwEcRuWnx7w98-AA
    latitude: 
    longitude: 
---
Depuis peu, ils se sont entourés de coquettes… ou plutôt de poulettes, qui produisent des œufs de pâturage, tout en profitant de l’abri d’un poulailler…mobile. Daniel et Suzanne font en effet du bien-être animal leur priorité.

Ils mettent ainsi en œuvre toute technique allant dans ce sens : pâturage, paillot, rations optimales pour l’éthologie des animaux avant les objectifs de performance (à titre d’exemple, intégration de paille et d’avoine dans l’alimentation des vaches même pour celles en production), … le tout avec des ressources uniquement produites sur la ferme.”

Ils viennent de lancer un modèle innovant de production d’œufs Bio de qualité supérieure, respectueux de l’environnement et du bien-être animal, basé sur le pâturage à partir d’un poulailler mobile de 200 poules, déplacé chaque semaine. L’intérêt vis-à-vis du label Prix Juste au Producteur est de justifier les coûts de production légèrement supérieurs de leurs œufs, en y intégrant la rémunération du travail.
