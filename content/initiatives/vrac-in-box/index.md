---
title: "Vrac in Box"
date: 2021-08-20T17:59:04+02:00
draft: false
longText: false
categories: ["zero-dechets"]
tags: [Farine,Epices,Produits ménagers,Cosmétiques,Vrac,Vegan,Circuit court,Commerce équitable,Monnaie locale,Evénèment,Ateliers]
summary: "Vrac in Box est une épicerie zéro déchet de produits alimentaires bio et locaux. Elle vous  propose également des produits écologiques pour l’hygiène et l’entretien."
youtubeId: 
valheureux: false
contact: 
    website: "http://www.vracinbox.be"
    mail: "clara-line@vracinbox.be"
    facebook: "VracinBox"
    twitter: 
    instagram: "vracinbox"
locations:
  - name: "Vrac in Box"
    address: "Rue Florent Pirotte, 1<br>4430 Ans"
    hours: 
    phone: 
    googlePlaceId: ChIJ4760QN77wEcR5sdpjvMXD0g
    latitude: 
    longitude: 
---
