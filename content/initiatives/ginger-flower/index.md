---  
title: "Ginger Flower"
date: 2020-04-29T09:26:07+02:00
categories: [zz-autres]
tags: [Vente directe,Circuit court,Commerce équitable,Vente en ligne,Fleurs]
summary: >
    Fleuriste éco-responsable: bouquets, décoration de mariages et d'événements avec des fleurs 100% belges, de saison et sans pesticide. 
    Sur commande uniquement, livraison possible à domicile ou dans les nombreuses boutiques partenaires à Liège
youtubeId: ""
contact:
    website: "https://www.gingerflower.be"
    mail: "gingerflowerbe@gmail.com"
    facebook: gingerflowerbe
    twitter: ""
    instagram: "gingerflowerbe"
#locations:
#  - name: ""
#    address: ""
#    hours: ""
#    phone: ""
#    latitude: 
#    longitude: 
#    googlePlaceId: 
---