---
title: "Le Retour Du Savon"
date: 2020-07-08T10:04:42+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Cosmétiques,Vrac,Vegan,Circuit court,Commerce équitable,Vente en ligne]
summary: >
    Savons et shampoings maison saponifiés à froid
youtubeId: 
valheureux: true
contact: 
    website: https://www.leretourdusavon.com
    mail: leretourdusavon@gmail.com
    facebook: leretourdusavon
    twitter: 
    instagram: 
locations:
  - name: Le Retour du Savon
    address: Chaussée de Liège 110a/11<br> 4540 Amay
    hours: 
    phone: 0486 40 37 35
    googlePlaceId: ChIJBzyO782rwUcRBM62Fk3BFVk
    latitude: 
    longitude: 
---
Une explication très intéressante à lire, par Valérie (porteuse du projet)!


> # Pourquoi ce nom? #
>
> Il y a deux niveaux de lectures évidemment!
> De toute évidence il est temps pour le savon de reprendre la place qu'il n'aurait jamais dû perdre dans vos douches, sur vos baignoires et lavabos.
> Pas besoin de long discours à propos de l'économie d'emballage d'un savon sur un pot de gel douche ou de shampoing!
> Cependant il serait réducteur de limiter la réflexion à l'aspect "déchets" des choses.
>
> **Pour tous ceux qui en on marre de ne rien comprendre aux étiquettes ou qui (comme moi) perdent un temps fou à les déchiffrer,  le savon est de retour ! (Je n'ai pas osé "contre-attaque"...)**
>
> 
> Certes, un savon saponifié à froid n'a pas un pH neutre. Un processus de saponification donne par essence un résultat basique. Quand on parle de savon au "pH neutre" il s'agit en réalité de "savon sans savon" généralement à base de tensioactifs de synthèse. Ce genre de produit peut être intéressant pour des peaux particulièrement sensibles ou abîmées.
> Une peau en bonne santé rééquilibre cependant naturellement sans pH au bout d'une heure au deux.
> A priori, le principal problème qu'une peau saine va rencontrer lors du lavage c'est que la saleté va être éliminée mais sa protection naturelle, le **sébum**, va disparaître elle aussi.  C'est ici que les huiles non saponifiées et la glycérine du savon interviennent! Elles vont aider la peau à **récupérer la protection naturelle** qu'elle a perdue. Pour les fans de noms scientifiques, cela s'appelle reconstituer le film hydrolipidique.
> **Dernier point fort du savon:** il n'est pas liquide, il peut donc se conserver sans adjonction de conservateurs!
> 
> 
> Je vous entends déjà penser : *"Oui mais un savon naturel cela ne mousse pas"!*
> Vous n'avez pas entièrement tort de le penser. Il existe très peu d'huiles végétales qui moussent. Cependant des huiles comme l'huile de coco, l'huile de ricin ou l'huile de palmiste fournissent une mousse abondante.
> 
> Je fais partie des personnes qui, après avoir utilisé du savon d'alep consciencieusement pendant des semaines, en sont arrivées à la conclusion que, décidément, elles préféraient qu'un savon mousse un peu. Les savons que je propose contiennent donc de l'huile de coco ou de l'huile de ricin, voire les deux!
> 
