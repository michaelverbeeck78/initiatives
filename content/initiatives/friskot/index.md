---
title: "Friskot"
date: 2021-08-20T17:23:31+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Maraichage,Produits laitiers,Boissons,Farine,Boulangerie,Epices,Vrac,Vegan,Seconde main,Coopérative,Evénèment,Solidarité,Sensibilisation,Education,Ateliers]
summary: "Créé en 2017, le FRISKOT est un kot-à-projet liégeois organisé par des étudiant.e.s. Ils ont décidé de vivre ensemble en s’investissant bénévolement dans un projet commun : un frigo solidaire."
youtubeId: 
valheureux: true
contact: 
    website: "https://www.fgtb-liege.be/kot"
    mail: "friskotlg@gmail.com"
    facebook: "FriskotLg"
    twitter: 
    instagram: "friskot_liege"
locations:
  - name: "Friskot"
    address: "Rue St Léonard 332<br>4000 Liège"
    hours: 
    phone: 
    googlePlaceId: ChIJZzJsO0vxwEcRZg9UiUDNnzw 
    latitude: 
    longitude: 
---

La nouvelle équipe du Friskot a le plaisir de vous annoncer sa réouverture le lundi 5 octobre 2020 ! Elise, Aliou, Kevin, Cyrielle, Chillah, Valentina, Alex et Damien seront au 332 rue Saint Léonard pour vous y accueillir.

Un frigo solidaire, c’est un frigo accessible à toutes et tous dans lequel chacun peut venir y déposer ses excédents alimentaires ou y chercher ce qui lui manque, gratuitement bien-sûr. Le Friskot est une réponse économique et écologique pour lutter contre le gaspillage alimentaire et les inégalités sociales.

En Wallonie, en moyenne 23 kg de nourriture sont gaspillés chaque année par les citoyens. Le projet est né du constat qu’il était possible de diminuer le gaspillage alimentaire tout en proposant une alternative solidaire. Cette initiative sociale avait déjà vu le jour à l'étranger, à Berlin et à Londres.

Aujourd’hui, cela fait 4 ans que le Friskot a ouvert ses portes à Liège, au cœur du quartier Saint-Léonard. L’objectif n’est pas seulement de venir déposer ou chercher de la nourriture, c’est aussi de créer du lien social.

Le projet, initié et soutenu par la Centrale Jeunes de la FGTB Liège-Huy-Waremme, entend favoriser la mise en place d’alternatives citoyennes et militantes au sein de la ville de Liège. Par ailleurs, , le projet étoffe l’offre de logements accessibles pour étudiants. Pour ce faire, les Jeunes FGTB Liège-Huy-Waremme ont réalisé  un partenariat avec « les Tournières », une coopérative liégeoise d’investissements immobiliers éthiques et solidaires, afin de proposer aux étudiant.e.s un prix de loyer décent.

Les Jeunes FGTB Liège-Huy-Waremme veulent permettre aux étudiant.e.s de devenir des acteur.trice.s critiques, solidaires et responsables de la société tout en développant des compétences en parallèle de leurs études (gestion de projet, travail en équipe, insertion dans la vie associative,…).

N’hésitez-pas à pousser la porte de leur demeure. Consultez leurs pages Facebook et Instagram pour connaître les dernières mises à jour, les horaires d’ouverture, les évènements et la liste des aliments mis à disposition.

Photos: Elise Lesenfants
