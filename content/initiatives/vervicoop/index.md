---
title: "Vervicoop"
date: 2021-08-20T16:32:05+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Maraîchage,Produits laiters,Boissons,Farine,Boulangerie,Epices,Produits ménagers,Cosmétiques,Vrac,Circuit court,Commerce équitable,Evénèment,Sensibilisation,Ateliers]
summary: "Vervîcoop est un *super*marché coopératif à but non lucratif et à finalité sociale."
youtubeId: 
valheureux: false
contact: 
    website: "https://vervicoop.be/"
    mail: "info@vervicoop.be"
    facebook: "vervicoop"
    twitter: 
    instagram: "vervicoop"
locations:
  - name: "Vervîcoop" 
    address: "Rue de Heusy 30<br>4800 Verviers" 
    hours: 
    phone: 
    googlePlaceId: ChIJE1Ei4J6LwEcRyHRjoGKdwm0 
    latitude: 
    longitude:
---
Vervîcoop est un "super"marché coopératif à but non lucratif et à finalité sociale. 

Il est géré par ses membres, pour ses membres : les coopérateurs. 
La participation active de tous les membres-coopérateurs à la gestion de l’épicerie permet l’accès à des produits de qualité, prioritairement issus de l’agriculture durable et de circuits courts. Ce mode de fonctionnement favorise l’économie locale et permet de payer les producteurs au prix juste. 

Tout en redonnant du sens à la consommation, Vervîcoop souhaite donner naissance à un lieu d’échanges, de solidarité, de partage et de sensibilisation aux enjeux alimentaires actuels.
