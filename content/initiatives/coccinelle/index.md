---
title: "Coccinelle"
date: 2020-08-12T11:15:50+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Maraîchage,Produits laitiers,Boissons,Farine,Boulangerie,Epices,Produits ménagers,Cosmétiques,Vrac,Vegan,Vente directe,Circuit court,Commerce équitable,En ligne,Ateliers]
summary: >
    La Coccinelle, L'épicerie de bon sens! Elle est née du désir de ses 2 fondatrices qui rêvent d'un monde plus sain et plus simple.
youtubeId: 
valheureux: false
contact: 
    website: http://lacoccinelle.bio/wordpress/
    mail: bea@lacoccinelle.bio
    facebook: LaCoccinelleFleron
    twitter: 
    instagram: 
locations:
  - name: Coccinelle
    address: Avenue des Martyrs 210<br>4620 Fléron
    hours: 
    phone: 04 339 79 05
    googlePlaceId: ChIJNxgD-Fb3wEcRg-7go6xJ1xA
    latitude: 
    longitude: 
---
C'est dans un cadre chaleureux que vous y trouverez un large choix de produits locaux, vrac et éthiques.
Il y a tout un tas de sortes de riz, pâtes, céréales, légumineuses, fruits secs, biscuits et friandises, des produits laitiers, des fruits et légumes frais et de saison. De quoi varier les plaisirs.

Des produits de cosmétiques belges ainsi que des produits d'entretien de base, pour faire ses recettes soi-même, ou prêt à l'emploi.
Et toujours le conseil!

Laissez-vous surprendre par le #bonsens

# A vos sacs, à vos bocaux !
