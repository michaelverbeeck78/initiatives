---
title: "L'Abeille Au Bois Dormant"
date: 2020-08-12T11:19:49+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Apiculture,Elevage,Boissons,Cosmétiques,Vente directe,Circuit court,Commerce équitable,Vente en ligne,Sensibilisation,Fleurs]
summary: >
    L'abeille au bois dormant est une petite entreprise Apicole locale et artisanale. Nous produisons différents produits autour du monde des abeilles. Miel, baumes à lèvres, bougies parfumées, cire d'abeille, crèmes de jour, semences pour pollinisateurs, rhum au miel, savons au miel, propolis.
youtubeId: 
valheureux: false
contact: 
    website: https://abeille-au-bois-dormant.shop/
    mail: 
    facebook: labeilleauboisdormant
    twitter: 
    instagram: 
locations:
  - name: L'Abeille au Bois Dormant
    address: Lagrange 16<br> 4160 Anthisnes
    hours: 
    phone: 
    googlePlaceId: ChIJ_xHoi9FXwEcR8RvA-J9Qxsg
    latitude: 
    longitude: 
---
*Illustration: Amaury Renard*