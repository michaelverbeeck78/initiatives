---
title: "La Ferme à l'Arbre"
date: 2021-08-20T16:12:07+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Maraîchage,Elevage,Produits laitiers,Boissons,Farine,Boulangerie,Cosmétiques,Vrac,Vegan,Vente directe,Circuit-court,Commerce équitable,Restauration,Monnaie locale]
summary: "Ferme 100% bio - Boucherie 100% bio - Magasin 100% bio. Vous cherchez un magasin d’alimentation convivial, spécialisé et durable? Vous êtes à la bonne adresse !"
youtubeId: 
valheureux: false
contact: 
    website: "http://www.ferme-paque.be"
    mail: contact@ferme-paque.be
    facebook: La-Ferme-%C3%A0-lArbre-de-Li%C3%A8ge-194830993932063
    twitter: 
    instagram: 
locations:
  - name: La Ferme à l'Arbre
    address: Rue de Liège 45<br>4450 Lantin
    hours: 
    phone: 04 263 58 01
    googlePlaceId: ChIJG-MXyRH7wEcR15ZCNYjZmsg
    latitude: 
    longitude: 
---

Convivial parce qu’avec sa conception écologique et harmonieuse, il invite au contact. Ses 350 m² permettent un assortiment très complet de produits bio sur un espace à taille humaine, où tout est à portée de la main. 

Spécialisé parce qu’on y retrouve tout le bio avec dans chaque domaine des collaborateurs compétents, prêts à vous aider et vous conseiller. En alimentation, durable sous-entend bio et local - ça tombe bien, ici on fait les deux. 
Et pour de nombreux produits, vous êtes carrément chez le producteur !
