---
title: "Epiceries"
subtitle: "Epiceries"
colorId: 1
summary: >
    Une carte des épiceries de Liège et ses environs sera disponible prochainement! 
---
En attendant, nous vous conseillons de consulter celle du Val'heureux disponible à l'adresse https://prestataires.valheureux.be  
  
**Projets en cours de contact:** Al'binète, La ruche qui dit oui, Néblon le bio, La Petite gatte, Vert2terre, Vrac'OliBri. [Vous en connaissez d'autres?](https://forms.gle/aK9C8uPXudokhfUP7)
