---
title: Producteurs
subtitle: Producteurs
colorId: 4
summary: >
    Ce sont les héros de la Transition: partir à la découverte des producteurs de la région permet de les soutenir et de profiter de toute la richesse de leurs produits. Acheter  directement chez le producteur, c'est favoriser un échange direct, sans intermédiaire! 
---
**Projets en cours de contact:** Au four et au jardin, Bergers de la Haze, Brasserie Libertas Gentis, Ferme de Desnié, Ferme de la Neuville, Ferme de targnon, Ferme Delarbre, Jardin du Mont Pointu, Jardins du Sart, Le fruitier, Les Pousses Poussent, Les sentiers du potager, Lî Cortis des Fawes, Ferme du Saule Penché, Terre d'Herbage SCRL. 

Nous préparons des **circuits de producteurs**: l'idée est de rencontrer plusieurs producteurs locaux d'une région en une demi-journée, une façon de joindre l'utile à l'agréable! 
*Suivez nos pages Facebook et Instagram pour être au courant de nos mises à jour.*

