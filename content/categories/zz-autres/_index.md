---
title: "Autres"
subtitle: Et les autres
colorId: 3
summary: >
    Mobilité douce, éducation et sensibilisation, artisanat, horeca...Il existe autant de visages à la Transition qu'il y a de métiers qui construisent la société de demain.   
---
**Projets en cours de contact:** Barricade, CRIE, De la terre à l'assiette, Heid de Frenay, Il était une fée, Jardins collectifs et potagers communautaires du Parc St-Agathe, Maison Vaillant-Wathelet, Masse Critique, Pépinière de la Prêle, Pierre qui roule, Potager communautaire au sein de l’Ilot Firquet / Saint-Séverin, Potager des Forges, Potager et verger communautaire rue de l’Arbre Courte Joie, Studio Colibri, Urbn'Bee.

**Pour le secteur Horeca:** Grand Maison, Les Voisines, Ma ferme en ville, l'Enoteca, Le Chaudron. 
